<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of gestion_usuarios.php
 *
 * @author Víctor Perete <fusiodarts.com>
 * Contacto: contacto@fusiodarts.com
 */
class gestion_usuarios extends fs_model
{

    public $codcuenta;
    public $codcliente;
    public $objetivocuenta;
    public $descripcioncuenta;
    public $emailcuenta;
    public $usuariocuenta;
    public $contrasenyacuenta;
    public $obscuenta;

    public function __construct($c = FALSE)
    {
        parent::__construct('gestionusuarioscli', 'plugins/gestion_usuarioscli/');

        if ($c)
        {
            if (!isset($c['codcuenta']) || isset($c['codcuenta']) == 0)
            {
                $this->codcuenta = $this->generar_codcuenta();
            } else
            {
                $this->codcuenta = $c['codcuenta'];
            }

            $this->codcliente        = $c['codcliente'];
            $this->objetivocuenta    = $c['objetivocuenta'];
            $this->descripcioncuenta      = $c['descripcioncuenta'];
            $this->emailcuenta       = $c['emailcuenta'];
            $this->usuariocuenta     = $c['usuariocuenta'];
            $this->contrasenyacuenta  = $c['contrasenyacuenta'];
            $this->obscuenta         = $c['obscuenta'];
        } else
        {
            $this->codcuenta         = NULL;
            $this->codcliente        = NULL;
            $this->objetivocuenta    = NULL;
            $this->descripcioncuenta      = NULL;
            $this->emailcuenta       = NULL;
            $this->usuariocuenta     = NULL;
            $this->contrasenyacuenta  = NULL;
            $this->obscuenta         = NULL;
        }
    }

    protected function install()
    {
        return '';
    }

    public function delete()
    {
        $sql = "DELETE FROM " . $this->table_name . " WHERE codcuenta = " . $this->var2str($this->codcuenta) . ";";
        if ($this->db->exec($sql))
        {
            header('Location: index.php?page=gestion_usuarioscli');
            return TRUE;
        } else
            return FALSE;
    }

    public function exists() {
        if ($this->db->select("SELECT codcuenta FROM " . $this->table_name . " WHERE codcuenta = " . $this->var2str($this->codcuenta) . ";")) {
            return TRUE;
        }

        return FALSE;
    }

    public function save()
    {
        if ($this->exists())
        {
            $sql = "UPDATE " . $this->table_name . " SET "
                    . "codcliente = " . $this->var2str($this->codcliente)
                    . ", objetivocuenta = " . $this->var2str($this->objetivocuenta)
                    . ", descripcioncuenta = " . $this->var2str($this->descripcioncuenta)
                    . ", emailcuenta = " . $this->var2str($this->emailcuenta)
                    . ", usuariocuenta = " . $this->var2str($this->usuariocuenta)
                    . ", contrasenyacuenta = " . $this->var2str($this->contrasenyacuenta)
                    . ", obscuenta = " . $this->var2str($this->obscuenta)
                    . " WHERE codcuenta = " . $this->var2str($this->codcuenta) . ";";
        } else
        {
            $sql = "INSERT INTO " . $this->table_name . " (codcuenta,codcliente,
                    objetivocuenta,descripcioncuenta,emailcuenta,usuariocuenta,contrasenyacuenta,obscuenta)
                    VALUES (" . $this->var2str($this->codcuenta) . "," . $this->var2str($this->codcliente) . ",
                    " . $this->var2str($this->objetivocuenta) . "," . $this->var2str($this->descripcioncuenta) . "," . $this->var2str($this->emailcuenta) . ",
                    " . $this->var2str($this->usuariocuenta) . "," . $this->var2str($this->contrasenyacuenta) . ",
                    " . $this->var2str($this->obscuenta) . ");";
        }

        if ($this->db->exec($sql))
        {
            return TRUE;
        } else
            return FALSE;
    }

    public function get($codcuenta)
    {
        $data = $this->db->select("SELECT * FROM " . $this->table_name . " WHERE codcuenta = " . $this->var2str($codcuenta) . " ORDER BY codcuenta ASC;");
        if ($data)
        {
            return new gestion_usuarios($data[0]);
        } else
            return FALSE;
    }

    public function all($offset = 0, $limit = FS_ITEM_LIMIT)
    {
        $clist = array();

        $data = $this->db->select_limit("SELECT * FROM " . $this->table_name . " ORDER BY codcuenta DESC", $limit, $offset);
        if ($data)
        {
            foreach ($data as $d)
                $clist[] = new gestion_usuarios($d);
        }

        return $clist;
    }

    public function url()
    {
        if (is_null($this->codcuenta))
        {
            return 'index.php?page=gestion_usuarioscli';
        } else
            return 'index.php?page=gestion_usuarioscli&codcuenta=' . $this->codcuenta;
    }

    public function search($query = '', $codcuenta = NULL, $objetivocuenta = NULL, $codcliente = NULL)
    {
        $query = $this->no_html(strtolower($query));
        $sql   = "SELECT * FROM " . $this->table_name . " WHERE";

        if ($query != '')
        {
            $sql .= " lower(codcuenta) LIKE '%" . $query . "%' OR lower(objetivocuenta) LIKE '%" . $query . "%' OR lower(codcliente) LIKE '%" . $query . "%'";
        } else
            $sql .= " 1 = 1";

        $sql .= " ORDER BY codcuenta ASC;";
        $dlist = array();

        $data = $this->db->select($sql);
        if ($data)
        {
            foreach ($data as $d)
                $dlist[] = new gestion_usuarios($d);
        }

        return $dlist;
    }

    public function all_from_cliente($codcliente, $offset = 0)
    {
        $clist  = array();
        $cuentas = $this->db->select_limit("SELECT * FROM " . $this->table_name .
                " WHERE codcliente = " . $this->var2str($codcliente) .
                " ORDER BY codcuenta DESC", FS_ITEM_LIMIT, $offset);
        if ($cuentas)
        {
            foreach ($cuentas as $c)
                $clist[] = new gestion_usuarios($c);
        }
        return $clist;
    }
    public function all_from_objetivocuenta($objetivocuenta, $offset = 0)
    {
        $clist  = array();
        $cuentas = $this->db->select_limit("SELECT * FROM " . $this->table_name .
                " WHERE objetivocuenta = " . $this->var2str($objetivocuenta) .
                " ORDER BY objetivocuenta DESC, codcuenta DESC", FS_ITEM_LIMIT, $offset);
        if ($cuentas)
        {
            foreach ($cuentas as $c)
                $clist[] = new gestion_usuarios($c);
        }
        return $clist;
    }
    public function all_from_emailcuenta($emailcuenta, $offset = 0)
    {
        $clist  = array();
        $cuentas = $this->db->select_limit("SELECT * FROM " . $this->table_name .
                " WHERE emailcuenta = " . $this->var2str($emailcuenta) .
                " ORDER BY objetivocuenta DESC, codcuenta DESC", FS_ITEM_LIMIT, $offset);
        if ($cuentas)
        {
            foreach ($cuentas as $c)
                $clist[] = new gestion_usuarios($c);
        }
        return $clist;
    }

    public function all_from_estudio($codestudio, $offset = 0)
    {
        $clist  = array();
        $cuentas = $this->db->select_limit("SELECT * FROM " . $this->table_name .
                " WHERE codestudio = " . $this->var2str($codestudio) .
                " ORDER BY codcuenta ASC", FS_ITEM_LIMIT, $offset);
        if ($cuentas)
        {
            foreach ($cuentas as $c)
                $clist[] = new gestion_usuarios($c);
        }
        return $clist;
    }

    public function generar_codcuenta()
    {
        $sql   = "SELECT COALESCE(MAX(codcuenta),'0') as max FROM " . $this->table_name . ";";
        $codcu = $this->db->select($sql);
        $res    = $codcu[0]['max'] + 1;

        return str_pad($res, 6, "0", STR_PAD_LEFT);
    }

}
