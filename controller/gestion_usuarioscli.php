<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of gestion_usuarioscli.php
 *
 * @author Víctor Perete <fusiodarts.com>
 * Contacto: contacto@fusiodarts.com
 */

require_model('gestion_usuarios.php');
require_model('cliente.php');
require_model('registro_sat.php');
require_model('detalle_sat.php');
require_model('agente.php');
require_model('articulo.php');
require_model('servicio_cliente.php');
require_model('estado_servicio.php');
require_model('detalle_servicio.php');

class gestion_usuarioscli extends fs_controller {

    public $listadocuentas;
    public $cuenta;
    public $listadoclientes;
    public $cliente;
    public $clientes_s;
    public $iframe;
    public $objetivo;
    public $email;
    public $modal;
    public $codcliente;

    public function __construct() {
        parent::__construct(__CLASS__, 'Gestion usuarios de clientes', 'ventas', FALSE, TRUE);
    }

    protected function private_core() {

        $this->share_extensions();
        $this->cuenta = new gestion_usuarios();
        $this->cliente = new cliente();
        $this->clientes_s = FALSE;
        $this->modal = 0;

        $this->listadoclientes = $this->cliente->all_full();
        
        if (isset($_REQUEST['iframe']) && $_REQUEST['iframe'] != '') {
            $this->iframe = $_REQUEST['iframe'];
        }        

        if ((isset($_REQUEST['codcliente_s']) && ($_REQUEST['codcliente_s'] != '')) || isset($_GET['codcliente']) && ($_GET['codcliente'] != '')) {
            if (isset($_REQUEST['codcliente_s']) && ($_REQUEST['codcliente_s'] != '')){
                $this->codcliente = $_REQUEST['codcliente_s'];
                $this->clientes_s = $this->cliente->get($_REQUEST['codcliente_s']);
                $this->listadocuentas = $this->cuenta->all_from_cliente($_REQUEST['codcliente_s']);
            } else {
                $this->codcliente = $_REQUEST['codcliente'];
                $this->clientes_s = $this->cliente->get($_REQUEST['codcliente']);
                $this->listadocuentas = $this->cuenta->all_from_cliente($_REQUEST['codcliente']);
            }
        } else {
            if (isset($_REQUEST['objetivocuenta_s']) && $_REQUEST['objetivocuenta_s'] != '') {
                $this->listadocuentas = $this->cuenta->all_from_objetivocuenta($_REQUEST['objetivocuenta_s']);
                $this->objetivo = $this->listadocuentas[0]->objetivocuenta;
            } else if (isset($_REQUEST['emailcuenta_s']) && $_REQUEST['emailcuenta_s'] != '') {
                $this->listadocuentas = $this->cuenta->all_from_emailcuenta($_REQUEST['emailcuenta_s']);
                $this->email = $this->listadocuentas[0]->emailcuenta;
            } else {
                $this->listadocuentas = $this->cuenta->all();
            }
        }
        
        foreach ($this->listadocuentas as $i => $lc) {
            if (strpos($lc->descripcioncuenta, 'http') !== false) {
                $lc->descripcioncuenta = '<a href="' . $lc->descripcioncuenta . '" target="_blank">' . $lc->descripcioncuenta . '</a>';
            }
            
            if (strpos($lc->descripcioncuenta, 'www.') !== false) {
                $lc->descripcioncuenta = '<a href="http://' . $lc->descripcioncuenta . '" target="_blank">' . $lc->descripcioncuenta . '</a>';
            }
        }
        
        if(!isset($_POST['objetivocuenta']) && isset($_POST['codcliente']) && ($_POST['codcliente'] != '')) {
            header("Location:index.php?page=gestion_usuario&codcliente=$_POST[codcliente]&iframe=''&ac_cliente=$_POST[ac_cliente]");
        }else if (isset($_POST['codcliente']) && $_POST['codcliente']==''){
            $this->modal = 1;
        }

        /*
         * CUENTAS: si recibimos por POST el codigo de la cuenta, queremos guardarlo
         * $_POST['codcuenta'] = 'Objetivo de la cuenta' -> variable string con el objetivo de la cuenta
         * 
         */
        if (isset($_POST['objetivocuenta']) && $_POST['objetivocuenta'] != '' && isset($_POST['codcuenta']) && ($_POST['codcuenta'] != '')) {
            $this->cuenta = new gestion_usuarios($_POST);

            if ($this->cuenta->save()) {
                if($this->iframe==0)
                header("Location:index.php?page=gestion_usuarioscli&iframe=0");
                else
                    header("Location:index.php?page=gestion_usuarioscli&iframe=1&codcliente_s={$_REQUEST[codcliente]}");
                $this->new_message("Datos de la cuenta guardados correctamente");
            } else
                $this->new_error_msg("¡Error al guardar la cuenta!");
        }else if (isset ($_REQUEST['codcuenta'])) {
            if (!isset($_POST['codcliente']) || $_POST['codcliente'] == '')
                $this->new_error_msg("¡Error al guardar la cuenta, se debe seleccionar un cliente válido!");
            else
                $this->new_error_msg("¡Error al guardar la cuenta se debe escribir un objetivo!");
        }
        if (isset($_REQUEST['buscar_cliente'])) {
            $this->buscar_cliente();
        }
        if (isset($_REQUEST['buscar_objetivocuenta'])) {
            $this->buscar_objetivocuenta();
        }
        if (isset($_REQUEST['buscar_emailcuenta'])) {
            $this->buscar_emailcuenta();
        }
        
        if (isset($_GET['delete']) && $_GET['delete']==1) {
            $delete = new gestion_usuarios();
            $delete->codcuenta = $_GET['codcuenta'];
            $delete->delete();
        }
    }

    public function get_client_name($codclient) {
        foreach ($this->listadoclientes as $value) {
            if ($value->codcliente == $codclient) {
                $name = $value->nombre;
            }
        }
        return $name;
    }

    private function buscar_objetivocuenta() {
        /// desactivamos la plantilla HTML
        $this->template = FALSE;

        $json = array();
        foreach ($this->cuenta->search($_REQUEST['buscar_objetivocuenta']) as $cuen) {
            $json[] = array('value' => $cuen->objetivocuenta, 'data' => $cuen->objetivocuenta);
        }

        header('Content-Type: application/json');
        echo json_encode(array('query' => $_REQUEST['buscar_objetivocuenta'], 'suggestions' => $json));
    }

    private function buscar_emailcuenta() {
        /// desactivamos la plantilla HTML
        $this->template = FALSE;

        $json = array();
        foreach ($this->cuenta->search($_REQUEST['buscar_emailcuenta']) as $cuen) {
            $json[] = array('value' => $cuen->emailcuenta, 'data' => $cuen->emailcuenta);
        }

        header('Content-Type: application/json');
        echo json_encode(array('query' => $_REQUEST['buscar_emailcuenta'], 'suggestions' => $json));
    }

    private function editar_cuenta() {
        /// desactivamos la plantilla HTML
        $this->template = FALSE;

        $json = array();
        $cuenta = $this->cuenta->get($_GET['editarcuenta']);

        header('Content-Type: application/json');
        echo json_encode($cuenta);
    }

    private function buscar_cliente() {
        /// desactivamos la plantilla HTML
        $this->template = FALSE;

        $json = array();
        foreach ($this->cliente->search($_REQUEST['buscar_cliente']) as $cli) {
            $json[] = array('value' => $cli->nombre . ' - (' . $cli->codcliente . ')', 'data' => $cli->codcliente);
        }

        header('Content-Type: application/json');
        echo json_encode(array('query' => $_REQUEST['buscar_cliente'], 'suggestions' => $json));
    }

    /* private function buscar_cliente() {
      /// desactivamos la plantilla HTML
      $this->template = FALSE;

      $cli0 = new cliente();
      $json = array();
      foreach ($cli0->search($_REQUEST['buscar_clientes']) as $cli) {
      $json[] = array('value' => $cli->nombre, 'data' => $cli->codcliente);
      }

      header('Content-Type: application/json');
      echo json_encode(array('query' => $_REQUEST['buscar_cliente'], 'suggestions' => $json));
      } */

    private function share_extensions() {
        /// añadimos las extensiones para usuarios
        $extensiones = array(
            array('name' => 'gestion_usuariocli_estudio',
                'page_from' => __CLASS__,
                'page_to' => 'proyectosweb_estudio',
                'type' => 'tab',
                'text' => '<span class="fa fa-user-secret"></span><span class="hidden-xs">&nbsp; Gestión de usuarios</span>',
                'params' => ''
            ),
            array(
                'name' => 'gestion_usuariocli_ventas_cliente',
                'page_from' => __CLASS__,
                'page_to' => 'ventas_cliente',
                'type' => 'button',
                'text' => '<span class="fa fa-user-secret">&nbsp;</span><span class="hidden-xs">&nbsp; Gestión de usuarios</span>',
                'params' => ''
            ),
        );
        /// añadimos las extensiones
        foreach ($extensiones as $ext) {
            $fsext = new fs_extension($ext);
            if (!$fsext->save()) {
                $this->new_error_msg('Error al guardar la extensión ' . $ext['name']);
            }
        }
    }

}
