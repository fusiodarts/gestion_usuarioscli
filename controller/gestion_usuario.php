<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of gestion_usuario.php
 *
 * @author Víctor Perete <fusiodarts.com>
 * Contacto: contacto@fusiodarts.com
 */

require_model('gestion_usuarios.php');
require_model('cliente.php');

class gestion_usuario extends fs_controller {

    public $cuenta;
    public $iframe;
    public $cliente;
    public $cliente_s;

    public function __construct() {
        parent::__construct(__CLASS__,"gestion_usuario", "ventas", FALSE, FALSE);
    }

    protected function private_core() {

        $this->cuenta = new gestion_usuarios();
        $this->iframe = $_REQUEST['iframe'];
        $this->cliente = new cliente();
        $this->cliente_s = $this->cliente->get($_REQUEST['codcliente']);

        if (isset($_GET['editarcuenta'])) {
            $this->cuenta = $this->cuenta->get($_GET['editarcuenta']);
        } else if (isset ($_REQUEST['codcliente'])) {
            //Recoge el codigo del cliente separando la variable que recibimos y quedandose la primera parte
            //$codcliente = explode(" - ", $_POST['ac_cliente']);
            $this->cuenta->codcliente = $_REQUEST['codcliente'];
            // Genera un 'codcuenta'
            $this->cuenta->codcuenta = $this->cuenta->generar_codcuenta();
        }  else {
            if(isset($_REQUEST['page'])&&$_REQUEST['page']=="admin_home"){
                header("Location:index.php?page=gestion_usuarioscli");
            }else{
                header("Location:index.php?page=gestion_usuarioscli&codcuenta=''");
            }
        }

    }
}
